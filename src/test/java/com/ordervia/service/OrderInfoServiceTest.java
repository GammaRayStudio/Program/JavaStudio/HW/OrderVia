package com.ordervia.service;

import com.ordervia.queue.datadef.ActionCode;
import com.ordervia.queue.model.OrderInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;


@SpringBootTest
class OrderInfoServiceTest {

    @Autowired
    private OrderInfoService orderInfoService;


    @Test
    public void commit() {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setId(1L);
        orderInfo.setActionCode(ActionCode.BUY);
        orderInfo.setPrice(1000L);
        orderInfo.setQuantity(10L);
        orderInfo.setOrderTime(new Date());
        orderInfoService.commit(orderInfo);

        System.out.println(orderInfoService.orderMsg());
    }

    @Test
    public void commit5Order(){
        OrderInfo orderInfo01 = new OrderInfo();
        orderInfo01.setId(1L);
        orderInfo01.setTradeId(1L);
        orderInfo01.setUserId(1L);
        orderInfo01.setActionCode(ActionCode.BUY);
        orderInfo01.setPrice(1000L);
        orderInfo01.setQuantity(10L);
        orderInfo01.setOrderTime(new Date());
        orderInfoService.commit(orderInfo01);

        OrderInfo orderInfo02 = new OrderInfo();
        orderInfo02.setId(2L);
        orderInfo02.setTradeId(2L);
        orderInfo02.setUserId(2L);
        orderInfo02.setActionCode(ActionCode.BUY);
        orderInfo02.setPrice(1000L);
        orderInfo02.setQuantity(5L);
        orderInfo02.setOrderTime(new Date());
        orderInfoService.commit(orderInfo02);

        OrderInfo orderInfo03 = new OrderInfo();
        orderInfo03.setId(3L);
        orderInfo03.setTradeId(3L);
        orderInfo03.setUserId(3L);
        orderInfo03.setActionCode(ActionCode.BUY);
        orderInfo03.setPrice(1000L);
        orderInfo03.setQuantity(3L);
        orderInfo03.setOrderTime(new Date());
        orderInfoService.commit(orderInfo03);

        OrderInfo orderInfo04 = new OrderInfo();
        orderInfo04.setId(4L);
        orderInfo04.setTradeId(1L);
        orderInfo04.setUserId(3L);
        orderInfo04.setActionCode(ActionCode.SELL);
        orderInfo04.setPrice(1000L);
        orderInfo04.setQuantity(5L);
        orderInfo04.setOrderTime(new Date());
        orderInfoService.commit(orderInfo04);

        OrderInfo orderInfo05 = new OrderInfo();
        orderInfo05.setId(5L);
        orderInfo05.setTradeId(1L);
        orderInfo05.setUserId(5L);
        orderInfo05.setActionCode(ActionCode.SELL);
        orderInfo05.setPrice(1000L);
        orderInfo05.setQuantity(3L);
        orderInfo05.setOrderTime(new Date());
        orderInfoService.commit(orderInfo05);

        System.out.println(orderInfoService.orderMsg());
    }



}