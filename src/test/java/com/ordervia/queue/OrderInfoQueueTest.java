package com.ordervia.queue;

import com.ordervia.queue.datadef.ActionCode;
import com.ordervia.queue.model.OrderInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class OrderInfoQueueTest {

    @Autowired
    private OrderInfoQueue orderInfoQueue;

    @Test
    public void trade(){
        OrderInfo orderInfo01 = new OrderInfo();
        orderInfo01.setId(1L);
        orderInfo01.setTradeId(1L);
        orderInfo01.setUserId(1L);
        orderInfo01.setActionCode(ActionCode.BUY);
        orderInfo01.setPrice(1000L);
        orderInfo01.setQuantity(10L);
        orderInfo01.setOrderTime(new Date());
        orderInfoQueue.push(orderInfo01);

        OrderInfo orderInfo02 = new OrderInfo();
        orderInfo02.setId(2L);
        orderInfo02.setTradeId(1L);
        orderInfo02.setUserId(2L);
        orderInfo02.setActionCode(ActionCode.SELL);
        orderInfo02.setPrice(1000L);
        orderInfo02.setQuantity(20L);
        orderInfo02.setOrderTime(new Date());
        orderInfoQueue.push(orderInfo02);

        System.out.println("積壓訂單: \n");
        System.out.println(orderInfoQueue.message() + "\n");

        System.out.println("處理交易: \n");
        orderInfoQueue.trade(1L);

        System.out.println("交易完成: \n");
        System.out.println(orderInfoQueue.message() + "\n");
    }

    @Test
    public void commit5Order(){
        OrderInfo orderInfo01 = new OrderInfo();
        orderInfo01.setId(1L);
        orderInfo01.setTradeId(1L);
        orderInfo01.setUserId(1L);
        orderInfo01.setActionCode(ActionCode.BUY);
        orderInfo01.setPrice(1000L);
        orderInfo01.setQuantity(10L);
        orderInfo01.setOrderTime(new Date());
        orderInfoQueue.push(orderInfo01);

        OrderInfo orderInfo02 = new OrderInfo();
        orderInfo02.setId(2L);
        orderInfo02.setTradeId(2L);
        orderInfo02.setUserId(2L);
        orderInfo02.setActionCode(ActionCode.BUY);
        orderInfo02.setPrice(2000L);
        orderInfo02.setQuantity(20L);
        orderInfo02.setOrderTime(new Date());
        orderInfoQueue.push(orderInfo02);

        OrderInfo orderInfo03 = new OrderInfo();
        orderInfo03.setId(3L);
        orderInfo03.setTradeId(3L);
        orderInfo03.setUserId(3L);
        orderInfo03.setActionCode(ActionCode.BUY);
        orderInfo03.setPrice(3000L);
        orderInfo03.setQuantity(30L);
        orderInfo03.setOrderTime(new Date());
        orderInfoQueue.push(orderInfo03);

        OrderInfo orderInfo04 = new OrderInfo();
        orderInfo04.setId(4L);
        orderInfo04.setTradeId(1L);
        orderInfo04.setUserId(3L);
        orderInfo04.setActionCode(ActionCode.SELL);
        orderInfo04.setPrice(1000L);
        orderInfo04.setQuantity(5L);
        orderInfo04.setOrderTime(new Date());
        orderInfoQueue.push(orderInfo04);

        OrderInfo orderInfo05 = new OrderInfo();
        orderInfo05.setId(5L);
        orderInfo05.setTradeId(1L);
        orderInfo05.setUserId(5L);
        orderInfo05.setActionCode(ActionCode.BUY);
        orderInfo05.setPrice(1000L);
        orderInfo05.setQuantity(5L);
        orderInfo05.setOrderTime(new Date());
        orderInfoQueue.push(orderInfo05);

        System.out.println("積壓訂單: \n");
        System.out.println(orderInfoQueue.message() + "\n");

        System.out.println("處理交易: \n");
        orderInfoQueue.trade(1L);

        System.out.println("交易完成: \n");
        System.out.println(orderInfoQueue.message() + "\n");
    }
}