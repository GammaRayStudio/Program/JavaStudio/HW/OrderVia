package com.ordervia.service;

import com.ordervia.queue.model.OrderInfo;
import org.springframework.stereotype.Service;

public interface OrderInfoService {
    /**
     * 提交訂單
     */
    void commit(OrderInfo orderInfo);

    /**
     * 積壓的訂單狀態
     */
    String orderMsg();
}
