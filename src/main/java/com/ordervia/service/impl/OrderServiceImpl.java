package com.ordervia.service.impl;

import com.ordervia.queue.OrderInfoQueue;
import com.ordervia.queue.model.OrderInfo;
import com.ordervia.service.OrderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderInfoService {

    @Autowired
    private OrderInfoQueue orderInfoQueue;

    public void commit(OrderInfo orderInfo){
        orderInfoQueue.push(orderInfo);
    }


    public String orderMsg(){
        return orderInfoQueue.message();
    }
}
