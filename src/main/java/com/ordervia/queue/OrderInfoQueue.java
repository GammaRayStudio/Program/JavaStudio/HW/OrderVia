package com.ordervia.queue;

import com.ordervia.queue.datadef.ActionCode;
import com.ordervia.queue.model.OrderInfo;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @Component 默認為單例模式
 */
@Component
public class OrderInfoQueue {
    private static Map<Long, Map<Long , Queue<OrderInfo>>> orderBuyMessage = new HashMap<>(); // 匹配清單(買) key: trade_id / val: <price , Queue>
    private static Map<Long, Map<Long , Queue<OrderInfo>>> orderSellMessage = new HashMap<>(); // 匹配清單(賣) key: trade_id / val: <price , Queue>
    private static Queue<Long> handleTradeId = new LinkedList<>();

    public OrderInfoQueue() {
    }

    /**
     * 生產者 : 訂單積壓
     */
    public void push(OrderInfo orderInfo){
        if(orderInfo.getActionCode() == ActionCode.BUY){ // 積壓買單
            if(orderBuyMessage.get(orderInfo.getTradeId()) != null){
                Map<Long , Queue<OrderInfo>> priceMessage = orderBuyMessage.get(orderInfo.getTradeId()); // 價格資訊 : 訂單列隊

                if(priceMessage.get(orderInfo.getPrice()) != null){
                    Queue<OrderInfo> lstOrder = priceMessage.get(orderInfo.getPrice());
                    lstOrder.offer(orderInfo);
                    priceMessage.put(orderInfo.getPrice() , lstOrder);

                }else{
                    Queue<OrderInfo> lstOrder = new LinkedList<>(); // 訂單隊列
                    lstOrder.add(orderInfo);

                    priceMessage.put(orderInfo.getPrice() , lstOrder);
                }

            }else{
                Queue<OrderInfo> lstOrder = new LinkedList<>(); // 訂單隊列
                lstOrder.offer(orderInfo);

                Map<Long , Queue<OrderInfo>> priceMessage = new HashMap<>(); // 價格資訊 : 訂單列隊
                priceMessage.put(orderInfo.getPrice() , lstOrder);

                orderBuyMessage.put(orderInfo.getTradeId() , priceMessage);
            }
        }


        if(orderInfo.getActionCode() == ActionCode.SELL){ // 積壓賣單
            if(orderSellMessage.get(orderInfo.getTradeId()) != null){
                Map<Long , Queue<OrderInfo>> priceMessage = orderSellMessage.get(orderInfo.getTradeId()); // 價格資訊 : 訂單列隊

                if(priceMessage.get(orderInfo.getPrice()) != null){
                    Queue<OrderInfo> lstOrder = priceMessage.get(orderInfo.getPrice());
                    lstOrder.offer(orderInfo);
                    priceMessage.put(orderInfo.getPrice() , lstOrder);

                }else{
                    Queue<OrderInfo> lstOrder = new LinkedList<>(); // 訂單隊列
                    lstOrder.add(orderInfo);

                    priceMessage.put(orderInfo.getPrice() , lstOrder);
                }

            }else{
                Queue<OrderInfo> lstOrder = new LinkedList<>(); // 訂單隊列
                lstOrder.offer(orderInfo);

                Map<Long , Queue<OrderInfo>> priceMessage = new HashMap<>(); // 價格資訊 : 訂單列隊
                priceMessage.put(orderInfo.getPrice() , lstOrder);

                orderSellMessage.put(orderInfo.getTradeId() , priceMessage);
            }

        }

    }

    /**
     * 消費者 : 處理訂單
     */
    // Enoxs Adjust : 取得訂單資訊與交易處理應該可以分開，此類別只供應訂單的物件
    public boolean trade(Long tradeId){
        System.out.println(" [交易代碼] : " + tradeId);
        boolean isMatch = false;
        try {
            if(orderBuyMessage.get(tradeId) != null && orderSellMessage.get(tradeId) != null){
                Map<Long , Queue<OrderInfo>> buyPriceMessage = orderBuyMessage.get(tradeId);
                Map<Long , Queue<OrderInfo>> sellPriceMessage = orderSellMessage.get(tradeId);

                List<Long> lstSellPrice = new ArrayList<>(sellPriceMessage.keySet());
                for (int i = 0; i < lstSellPrice.size(); i++) {
                    Long sellPrice = lstSellPrice.get(i);

                    System.out.println(" [交易價格] : " + sellPrice + "\n");

                    Queue<OrderInfo> lstSellOrder = sellPriceMessage.get(sellPrice);
                    if(buyPriceMessage.get(sellPrice) != null){ // 匹配成功
                        Queue<OrderInfo> lstBuyOrder = buyPriceMessage.get(sellPrice);

                        while(!lstSellOrder.isEmpty() && !lstBuyOrder.isEmpty()){
                            System.out.println(" sell.queue : " + lstSellOrder.size() + " , buy.queue : " + lstBuyOrder.size() + "\n");

                            OrderInfo sellOrder = lstSellOrder.peek();
                            OrderInfo buyOrder = lstBuyOrder.peek();

                            System.out.println("sell : " + sellOrder.getQuantity());
                            System.out.println("buy : " + buyOrder.getQuantity());

                            if(sellOrder.getQuantity().longValue() == buyOrder.getQuantity().longValue()){
                                sellOrder.setQuantity(0L);
                                buyOrder.setQuantity(0L);
                                System.out.println("完全匹配，完成交易\n");
                            }else if(sellOrder.getQuantity().longValue() > buyOrder.getQuantity().longValue()){
                                sellOrder.setQuantity(sellOrder.getQuantity().longValue() - buyOrder.getQuantity().longValue());
                                System.out.println("部分匹配，完成買單(清除)\n");
                            }else if(sellOrder.getQuantity().longValue() < buyOrder.getQuantity().longValue()){
                                buyOrder.setQuantity(buyOrder.getQuantity().longValue() - sellOrder.getQuantity().longValue());
                                System.out.println("部分匹配，完成賣單(清除)\n");
                            }

                            if(sellOrder.getQuantity() == 0L){
                                lstSellOrder.poll();
                            }

                            if(buyOrder.getQuantity() == 0L){
                                lstBuyOrder.poll();
                            }

                        }

                    }

                }
            }
        }catch(Exception e){
            e.printStackTrace();
            isMatch = false;
        }
        return isMatch;
    }


    /**
     * 檢查 : 待處理的賣單是否為空
     */
    public boolean isEmpty(){
        return handleTradeId.isEmpty();
    }

    /**
     * 取得 : 全部的賣單交易 id 長度
     */
    public int length(){
        return handleTradeId.size();
    }

    /**
     * 取得 : 待處理的賣單交易 id
     */
    public Long pollSellTradeId(){
        return handleTradeId.poll();
    }


    /**
     * 更新 : 待處理的交易 id
     */
    public void refreshTradeId(){
        if(!orderSellMessage.isEmpty()){
            List<Long> lstTradeId = new ArrayList<>(orderSellMessage.keySet());
            for (int i = 0; i < lstTradeId.size(); i++) {
                handleTradeId.offer(lstTradeId.get(i));
            }

        }
    }


    /**
     * 查詢 : 訂單現狀
     */
    public String message(){
        StringBuffer msg = new StringBuffer(32);
        List<Long> lstBuyId = new ArrayList<>(orderBuyMessage.keySet());
        List<Long> lstSellId = new ArrayList<>(orderSellMessage.keySet());

        msg.append("Buy : \n");
        for (int i = 0; i < lstBuyId.size(); i++) {
            msg.append("["+lstBuyId.get(i)+"]\n");
            List<Long> lstPrice = new ArrayList<>(orderBuyMessage.get(lstBuyId.get(i)).keySet());

            for (int j = 0; j < lstPrice.size(); j++) {
                if(orderBuyMessage.get(lstBuyId.get(i)).get(lstPrice.get(j)) != null){
                    msg.append(" " + lstPrice.get(j) + " : " + orderBuyMessage.get(lstBuyId.get(i)).get(lstPrice.get(j)).size() + "\n");
                }else{
                    msg.append(" " + lstPrice.get(j) + " : " + 0 + "\n");
                }
            }
        }

        msg.append("\n");
        msg.append("Sell : \n");
        for (int i = 0; i < lstSellId.size(); i++) {
            msg.append("["+lstSellId.get(i)+"]\n");
            List<Long> lstPrice = new ArrayList<>(orderSellMessage.get(lstSellId.get(i)).keySet());
            for (int j = 0; j < lstPrice.size(); j++) {
                if(orderSellMessage.get(lstSellId.get(i)).get(lstPrice.get(j)) != null){
                    msg.append(" " + lstPrice.get(j) + " : " + orderSellMessage.get(lstSellId.get(i)).get(lstPrice.get(j)).size() + "\n");
                }else{
                    msg.append(" " + lstPrice.get(j) + " : " + 0 + "\n");
                }
            }
        }

        return msg.toString();
    }

}
