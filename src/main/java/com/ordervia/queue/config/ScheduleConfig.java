package com.ordervia.queue.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;


@Configuration
@EnableAsync
public class ScheduleConfig{


    /**
     * 核心線程數(CorePoolSize) ---> （若全部被佔用） ---> 放入隊列(QueueCapacity)
     * 放入隊列(QueueCapacity) ---> （若全部被佔用） ---> 根據最大線程數(MaxPoolSize)創建新線程
     * 根據最大線程數(MaxPoolSize)創建新線程 ---> （若超過最大線程數） ---> 開始執行拒絕策略(RejectedExecutionHandler)
     */
    @Bean
    public Executor executor01() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("test-schedule2-"); // 配置線程池中的線程的名稱前綴
        executor.setMaxPoolSize(20); // 配置最大線程數
        executor.setCorePoolSize(15); // 配置核心線程數
        executor.setQueueCapacity(0); // 配置隊列大小
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy()); // 拒絕策略 : 拒絕後由調用者線程繼續執行
        return executor;
    }
}
