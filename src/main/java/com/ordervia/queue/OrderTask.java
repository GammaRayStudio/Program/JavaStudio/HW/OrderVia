package com.ordervia.queue;

import com.ordervia.queue.model.OrderInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderTask {

    @Autowired
    private OrderInfoQueue orderInfoQueue;

    private final static int maxLength = 3; // 為了方便觀察，限制設置小一點


    /**
     * 線程池處理訂單比對與交易
     */
    @Async("executor01") // 配置在 ScheduleConfig 裡面
    @Scheduled(fixedRate = 10000) // 間隔 10 秒處理一次訂單
    public void handleOrderData(){
        // 消化賣單
        // Enoxs Adjust : 取得訂單資訊與交易處理應該可以分開，此類別只處理訂單
        if(!orderInfoQueue.isEmpty()){
            int len = orderInfoQueue.length();

            if(len > maxLength){
                len = maxLength;
            }

            for (int i = 0; i < len; i++) {
                Long tradeId = orderInfoQueue.pollSellTradeId();
                if(tradeId != null){
                    boolean isMatch = orderInfoQueue.trade(tradeId);
                    if(isMatch){ // 配對成功 > 完成交易
                        System.out.println("訂單處理完成!");
                    }else{// 配對失敗 > 等待處置
                        System.out.println("訂單未匹配");
                    }

                }
            }
        }
    }

    /**
     * 單一線程 : 更新待處理的訂單 id
     */
    @Async
    @Scheduled(fixedRate = 30000)
    public void refreshTradeId(){
        System.out.println("refresh.");
        orderInfoQueue.refreshTradeId();
    }

}
