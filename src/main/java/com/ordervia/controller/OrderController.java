package com.ordervia.controller;

import com.ordervia.queue.datadef.ActionCode;
import com.ordervia.queue.model.OrderInfo;
import com.ordervia.service.OrderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderInfoService orderInfoService;

    // Enoxs Adjust : PostMapping
    @GetMapping("/commit")
    public void commit(@RequestBody OrderInfo orderInfo) {
        orderInfoService.commit(orderInfo);
    }


    @GetMapping("/test/commit3Order")
    public void commit3Order(){
        OrderInfo orderInfo01 = new OrderInfo();
        orderInfo01.setId(1L);
        orderInfo01.setTradeId(1L);
        orderInfo01.setUserId(1L);
        orderInfo01.setActionCode(ActionCode.BUY);
        orderInfo01.setPrice(1000L);
        orderInfo01.setQuantity(5L);
        orderInfo01.setOrderTime(new Date());
        orderInfoService.commit(orderInfo01);

        OrderInfo orderInfo02 = new OrderInfo();
        orderInfo02.setId(2L);
        orderInfo02.setTradeId(1L);
        orderInfo02.setUserId(1L);
        orderInfo02.setActionCode(ActionCode.BUY);
        orderInfo02.setPrice(2000L);
        orderInfo02.setQuantity(5L);
        orderInfo02.setOrderTime(new Date());
        orderInfoService.commit(orderInfo02);

        OrderInfo orderInfo03 = new OrderInfo();
        orderInfo03.setId(3L);
        orderInfo03.setTradeId(1L);
        orderInfo03.setUserId(2L);
        orderInfo03.setActionCode(ActionCode.SELL);
        orderInfo03.setPrice(1000L);
        orderInfo03.setQuantity(10L);
        orderInfo03.setOrderTime(new Date());
        orderInfoService.commit(orderInfo03);


        System.out.println(orderInfoService.orderMsg());
    }

    @GetMapping("/test/commit5Order")
    public void commit5Order(){
        OrderInfo orderInfo01 = new OrderInfo();
        orderInfo01.setId(1L);
        orderInfo01.setTradeId(1L);
        orderInfo01.setUserId(1L);
        orderInfo01.setActionCode(ActionCode.BUY);
        orderInfo01.setPrice(1000L);
        orderInfo01.setQuantity(10L);
        orderInfo01.setOrderTime(new Date());
        orderInfoService.commit(orderInfo01);

        OrderInfo orderInfo02 = new OrderInfo();
        orderInfo02.setId(2L);
        orderInfo02.setTradeId(2L);
        orderInfo02.setUserId(2L);
        orderInfo02.setActionCode(ActionCode.BUY);
        orderInfo02.setPrice(2000L);
        orderInfo02.setQuantity(20L);
        orderInfo02.setOrderTime(new Date());
        orderInfoService.commit(orderInfo02);

        OrderInfo orderInfo03 = new OrderInfo();
        orderInfo03.setId(3L);
        orderInfo03.setTradeId(3L);
        orderInfo03.setUserId(3L);
        orderInfo03.setActionCode(ActionCode.BUY);
        orderInfo03.setPrice(3000L);
        orderInfo03.setQuantity(30L);
        orderInfo03.setOrderTime(new Date());
        orderInfoService.commit(orderInfo03);

        OrderInfo orderInfo04 = new OrderInfo();
        orderInfo04.setId(4L);
        orderInfo04.setTradeId(1L);
        orderInfo04.setUserId(3L);
        orderInfo04.setActionCode(ActionCode.SELL);
        orderInfo04.setPrice(1000L);
        orderInfo04.setQuantity(5L);
        orderInfo04.setOrderTime(new Date());
        orderInfoService.commit(orderInfo04);

        OrderInfo orderInfo05 = new OrderInfo();
        orderInfo05.setId(5L);
        orderInfo05.setTradeId(1L);
        orderInfo05.setUserId(5L);
        orderInfo05.setActionCode(ActionCode.SELL);
        orderInfo05.setPrice(1000L);
        orderInfo05.setQuantity(3L);
        orderInfo05.setOrderTime(new Date());
        orderInfoService.commit(orderInfo05);

        System.out.println(orderInfoService.orderMsg());
    }

    @GetMapping("/message")
    public String orderMessage(){
        return orderInfoService.orderMsg();
    }

}
