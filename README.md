OrderVia
======
`處理交易，配對訂單`

```
Imagine you have a trade engine that accepts orders via the protocol (or triggering) you defined. An order request at least has this information (buy or sell, quantity, market price or limit price).
The engine matches buy and sell orders that have the same price. Orders have the same price determined by their timestamp (FIFO). Pending orders queue up in your system until they are filled or killed. Your system will have multiple traders executing orders at the same time.
```

語言
------
+ Java

框架
-------
+ SpringBoot

啟動
------
```
gradle bootRun
```

分層
------
+ controller `API`
+ service `訂單服務`
+ queue `訂單佇列與定時任務`

### Controller
**OrderController**
+ commit : 正常訂單的提交接口
+ commit3Order : 測試接口，驗證 3 筆提交
+ commit5Order : 測試接口，驗證 5 筆提交

### Service
**OrderInfoService**
+ commit : 提交訂單，加入佇列
+ orderMsg : 檢視積壓的訂單狀態

orderMsg , example :
```
Buy : 
[1]
 1000 : 1
[2]
 2000 : 1
[3]
 3000 : 1

Sell : 
[1]
 1000 : 2
```
代表買單交易 id 有三組(ex: 1,2,3)，價格分別 $1000,$2000,$3000
賣單交易 id 有一組，價格 $1000

### Queue
**model/OrderInfo**
```java
public class OrderInfo {
    private Long id;
    private Long tradeId; // 交易目標
    private Long userId; // 使用者 id
    private Integer actionCode; // 1 - buy , 2 - sell
    private Long quantity; // 數量
    private Long price; // 價格
    private Date orderTime; // 交易時間
}
```
+ 訂單物件，包含唯一值 id , 交易 id , 使用者 id , 交易代碼 , 交易數量 , 交易價格。

**datadef/ActionCode**
```java
public class ActionCode {
    public final static int BUY = 1;
    public final static int SELL = 2;
}
```

+ 訂單代碼，買與賣

**config/ScheduleConfig**
```java
@Configuration
@EnableAsync
public class ScheduleConfig{
    @Bean
    public Executor executor01() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("test-schedule2-");
        executor.setMaxPoolSize(20);
        executor.setCorePoolSize(15);
        executor.setQueueCapacity(0);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }
}
```
+ 配置定時任務使用的線程池內容，多執行緒處理。

**OrderInfoQueue**
```java
private static Map<Long, Map<Long , Queue<OrderInfo>>> orderBuyMessage = new HashMap<>(); 
private static Map<Long, Map<Long , Queue<OrderInfo>>> orderSellMessage = new HashMap<>(); 
```
+ 用 Map 與 Queue 紀錄訂單的資料結構
+ 第一層 Map , key 為 tradeId , value 為對應待處理的價格與訂單
+ 第二層 Map , key 為 price , value 為佇列存儲的訂單

```java
public void push(OrderInfo orderInfo){...}
```
+ 生產者 : 訂單積壓
+ 依據 tradeId 與 price 壓入到相對應的 queue 裡面

```java
public boolean trade(Long tradeId){...}
```
+ 消費者 : 處理訂單
+ 以傳入的 tradeId 為目標，處理所有相關交易價格的訂單匹配
  
```java
public boolean isEmpty(){...}
```
+ 檢查 : 待處理的賣單是否為空
+ 用於定時任務的多執行緒任務分配

```java
public int length(){...}
```
+ 取得 : 全部的賣單交易 id 長度    

```java
public Long pollSellTradeId(){...}
```
+ 取得 : 待處理的賣單交易 id

```java
public void refreshTradeId(){...}
```
+ 更新 : 待處理的交易 id    
+ 單一執行緒，定時觸發


```java
public String message(){...}
```
+ 查詢 : 訂單現狀
+ 片歷所有的 Map 與 Queue 內容
+ 回傳訊息與 OrderInfoService.orderMsg() 內容一致

example :
```
Buy : 
[1]
 1000 : 1
[2]
 2000 : 1
[3]
 3000 : 1

Sell : 
[1]
 1000 : 2
```
+ 代表買單交易 id 有三組(ex: 1,2,3)，價格分別 $1000,$2000,$3000
+ 賣單交易 id 有一組，價格 $1000

**OrderTask**
```java
@Component
public class OrderTask {
    private final static int maxLength = 3; // 為了方便觀察，限制設置小一點
    /**
     * 線程池處理訂單比對與交易
     */
    @Async("executor01") // 配置在 ScheduleConfig 裡面
    @Scheduled(fixedRate = 10000) // 間隔 10 秒處理一次訂單
    public void handleOrderData(){
        // 消化賣單
        if(!orderInfoQueue.isEmpty()){
            int len = orderInfoQueue.length();

            if(len > maxLength){
                len = maxLength;
            }

            for (int i = 0; i < len; i++) {
                Long tradeId = orderInfoQueue.pollSellTradeId();
                if(tradeId != null){
                    boolean isMatch = orderInfoQueue.trade(tradeId);
                    if(isMatch){ // 配對成功 > 完成交易
                        System.out.println("訂單處理完成!");
                    }else{// 配對失敗 > 等待處置
                        System.out.println("訂單未匹配");
                    }

                }
            }
        }
    }

    /**
     * 單一線程 : 更新待處理的訂單 id
     */
    @Async
    @Scheduled(fixedRate = 30000)
    public void refreshTradeId(){
        System.out.println("refresh.");
        orderInfoQueue.refreshTradeId();
    }
}
```
+ `maxLength` : 批次處理，限制數量為 3 
+ `@Async("executor01")` : 調用配置好的線程池處理
+ `@Scheduled(fixedRate = 10000)` : 間隔 10 秒處理訂單
+ `handleOrderData()` 處理邏輯 :
	+ 檢查 : 待處理的 tradeId Queue 是否不為空
	+ 檢查 : 最大不超過 maxLength
	+ 執行 : 批次取得 tradeId，呼叫 orderInfoQueue.trade() 方法
+ `refreshTradeId()` : 定時 30 秒，更新現存的 TradeId 


### Console
**cmd:**
```
enoxs@Enoxs-MBP OrderVia % curl 127.0.0.1:8080/order/test/commit5Order
enoxs@Enoxs-MBP OrderVia % curl 127.0.0.1:8080/order/test/commit5Order
enoxs@Enoxs-MBP OrderVia % curl 127.0.0.1:8080/order/test/commit5Order
enoxs@Enoxs-MBP OrderVia % curl 127.0.0.1:8080/order/test/commit5Order
enoxs@Enoxs-MBP OrderVia % curl 127.0.0.1:8080/order/test/commit5Order
```
+ 使用測試 API ，批次新增 25 筆測試訂單。

**log:**
```
commit5Order()
Buy : 
[1]
 1000 : 1
[2]
 2000 : 1
[3]
 3000 : 1

Sell : 
[1]
 1000 : 2

[2022-08-19T05:13:42.042Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-25] [268] [DEBUG] Using 'application/json', given [*/*] and supported [application/json, application/*+json, application/json, application/*+json]
[2022-08-19T05:13:42.042Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-25] [298] [DEBUG] Nothing to write: null body
[2022-08-19T05:13:42.042Z] [org.springframework.web.servlet.FrameworkServlet] [qtp1615598209-25] [1131] [DEBUG] Completed 200 OK
[2022-08-19T05:13:43.043Z] [org.springframework.core.log.LogFormatUtils] [qtp1615598209-20] [107] [DEBUG] GET "/order/test/commit5Order", parameters={}
[2022-08-19T05:13:43.043Z] [org.springframework.web.servlet.handler.AbstractHandlerMapping] [qtp1615598209-20] [522] [DEBUG] Mapped to com.ordervia.controller.OrderController#commit5Order()
Buy : 
[1]
 1000 : 2
[2]
 2000 : 2
[3]
 3000 : 2

Sell : 
[1]
 1000 : 4

[2022-08-19T05:13:43.043Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-20] [268] [DEBUG] Using 'application/json', given [*/*] and supported [application/json, application/*+json, application/json, application/*+json]
[2022-08-19T05:13:43.043Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-20] [298] [DEBUG] Nothing to write: null body
[2022-08-19T05:13:43.043Z] [org.springframework.web.servlet.FrameworkServlet] [qtp1615598209-20] [1131] [DEBUG] Completed 200 OK
[2022-08-19T05:13:43.043Z] [org.springframework.core.log.LogFormatUtils] [qtp1615598209-22] [107] [DEBUG] GET "/order/test/commit5Order", parameters={}
[2022-08-19T05:13:43.043Z] [org.springframework.web.servlet.handler.AbstractHandlerMapping] [qtp1615598209-22] [522] [DEBUG] Mapped to com.ordervia.controller.OrderController#commit5Order()
Buy : 
[1]
 1000 : 3
[2]
 2000 : 3
[3]
 3000 : 3

Sell : 
[1]
 1000 : 6

[2022-08-19T05:13:43.043Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-22] [268] [DEBUG] Using 'application/json', given [*/*] and supported [application/json, application/*+json, application/json, application/*+json]
[2022-08-19T05:13:43.043Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-22] [298] [DEBUG] Nothing to write: null body
[2022-08-19T05:13:43.043Z] [org.springframework.web.servlet.FrameworkServlet] [qtp1615598209-22] [1131] [DEBUG] Completed 200 OK
[2022-08-19T05:13:44.044Z] [org.springframework.core.log.LogFormatUtils] [qtp1615598209-24] [107] [DEBUG] GET "/order/test/commit5Order", parameters={}
[2022-08-19T05:13:44.044Z] [org.springframework.web.servlet.handler.AbstractHandlerMapping] [qtp1615598209-24] [522] [DEBUG] Mapped to com.ordervia.controller.OrderController#commit5Order()
Buy : 
[1]
 1000 : 4
[2]
 2000 : 4
[3]
 3000 : 4

Sell : 
[1]
 1000 : 8

[2022-08-19T05:13:44.044Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-24] [268] [DEBUG] Using 'application/json', given [*/*] and supported [application/json, application/*+json, application/json, application/*+json]
[2022-08-19T05:13:44.044Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-24] [298] [DEBUG] Nothing to write: null body
[2022-08-19T05:13:44.044Z] [org.springframework.web.servlet.FrameworkServlet] [qtp1615598209-24] [1131] [DEBUG] Completed 200 OK
[2022-08-19T05:13:44.044Z] [org.springframework.core.log.LogFormatUtils] [qtp1615598209-27] [107] [DEBUG] GET "/order/test/commit5Order", parameters={}
[2022-08-19T05:13:44.044Z] [org.springframework.web.servlet.handler.AbstractHandlerMapping] [qtp1615598209-27] [522] [DEBUG] Mapped to com.ordervia.controller.OrderController#commit5Order()
Buy : 
[1]
 1000 : 5
[2]
 2000 : 5
[3]
 3000 : 5

Sell : 
[1]
 1000 : 10

[2022-08-19T05:13:44.044Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-27] [268] [DEBUG] Using 'application/json', given [*/*] and supported [application/json, application/*+json, application/json, application/*+json]
[2022-08-19T05:13:44.044Z] [org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor] [qtp1615598209-27] [298] [DEBUG] Nothing to write: null body
[2022-08-19
```
+ 訂單新增，共 25 筆

**console:**
```
refresh.

 [交易代碼] : 1
 [交易價格] : 1000

 sell.queue : 10 , buy.queue : 5

sell : 5
buy : 10
部分匹配，完成賣單(清除)

 sell.queue : 10 , buy.queue : 5

sell : 5
buy : 5
完全匹配，完成交易

 sell.queue : 9 , buy.queue : 4

sell : 3
buy : 10
部分匹配，完成賣單(清除)

 sell.queue : 9 , buy.queue : 4

sell : 3
buy : 7
部分匹配，完成賣單(清除)

 sell.queue : 9 , buy.queue : 4

sell : 3
buy : 4
部分匹配，完成賣單(清除)

 sell.queue : 9 , buy.queue : 4

sell : 3
buy : 1
部分匹配，完成買單(清除)

 sell.queue : 9 , buy.queue : 4

sell : 2
buy : 1
部分匹配，完成買單(清除)

 sell.queue : 9 , buy.queue : 4

sell : 1
buy : 1
完全匹配，完成交易

 sell.queue : 8 , buy.queue : 3

sell : 5
buy : 10
部分匹配，完成賣單(清除)

 sell.queue : 8 , buy.queue : 3

sell : 5
buy : 5
完全匹配，完成交易

 sell.queue : 7 , buy.queue : 2

sell : 3
buy : 10
部分匹配，完成賣單(清除)

 sell.queue : 7 , buy.queue : 2

sell : 3
buy : 7
部分匹配，完成賣單(清除)

 sell.queue : 7 , buy.queue : 2

sell : 3
buy : 4
部分匹配，完成賣單(清除)

 sell.queue : 7 , buy.queue : 2

sell : 3
buy : 1
部分匹配，完成買單(清除)

 sell.queue : 7 , buy.queue : 2

sell : 2
buy : 1
部分匹配，完成買單(清除)

 sell.queue : 7 , buy.queue : 2

sell : 1
buy : 1
完全匹配，完成交易

 sell.queue : 6 , buy.queue : 1

sell : 5
buy : 10
部分匹配，完成賣單(清除)

 sell.queue : 6 , buy.queue : 1

sell : 5
buy : 5
完全匹配，完成交易
```

+ 每 30 秒，觸發 refreshTradeId()
+ 每 10 秒，觸發 handleOrderData()

**cmd:**
```
enoxs@Enoxs-MBP OrderVia % curl 127.0.0.1:8080/order/message          
Buy : 
[1]
 1000 : 0
[2]
 2000 : 5
[3]
 3000 : 5

Sell : 
[1]
 1000 : 5
```

+ 使用 message 接口，查看剩下的訂單資訊
+ tradeId 為 1 的買單，完全匹對消除
+ tradeId 為 1 的賣單，剩餘 5 筆訂單







